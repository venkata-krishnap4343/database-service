package com.databaseservice.Controller;

import com.databaseservice.Model.SignUpEntity;
import com.databaseservice.Model.SignUpRequest;
import com.databaseservice.Model.SignUpResponse;
import com.databaseservice.Service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Media;

@RestController
@RequestMapping(path="/dbservice/v1/")
@Slf4j
public class Controller {

    @Autowired
    UserService userService;

    @PostMapping(path = "signUp", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<SignUpResponse> signUp(@RequestHeader String txRoot,
                                 @RequestHeader String SystemId,
                                 @RequestBody SignUpRequest signUpRequest){
        log.info(" Data : txRoot : {}, SystemId : {}, UserData : {}",txRoot, SystemId, signUpRequest);
        SignUpResponse signUpResponse =userService.susnService(txRoot, SystemId, signUpRequest);
        return new ResponseEntity<>(signUpResponse, HttpStatus.CREATED);
    }
}
