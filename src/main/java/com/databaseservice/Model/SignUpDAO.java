package com.databaseservice.Model;


import lombok.Data;

import java.io.Serializable;

@Data
public class SignUpDAO implements Serializable {

    private Long Id;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private String password;
    private String address;
    private String university;
    private String designation;
    private String userId;
    private String team;
}
