package com.databaseservice.Model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity(name = "users")
@Table
public class SignUpEntity implements Serializable {

    @Id
    @Column(name = "Id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long Id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "address")
    private String address;

    @Column(name = "university")
    private String university;

    @Column(name = "designation")
    private String designation;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "team")
    private String team;

}
