package com.databaseservice.Model;

import lombok.Data;

@Data
public class SignUpResponse {
    private String userId;
    private String email;
}
