package com.databaseservice.Repository;

import com.databaseservice.Model.SignUpEntity;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<SignUpEntity, Long> {

    @Query(" from users u where u.email=:email")
    SignUpEntity findByemail(String email);
}
