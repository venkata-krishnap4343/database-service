package com.databaseservice.Service;


import com.databaseservice.Model.SignUpDAO;
import com.databaseservice.Model.SignUpEntity;
import com.databaseservice.Model.SignUpRequest;
import com.databaseservice.Model.SignUpResponse;
import com.databaseservice.Repository.UserRepository;
import com.databaseservice.Utils.Utilities;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    Utilities utilities;

    @Autowired
    UserRepository userRepository;

    public SignUpResponse susnService(String txRoot, String SystemId, SignUpRequest signUpRequest){

        ModelMapper modelMapper = new ModelMapper();

        SignUpDAO signUpDAO = modelMapper.map(signUpRequest, SignUpDAO.class);

        signUpDAO.setUserId(utilities.setUserId(signUpDAO.getFirstName(), signUpDAO.getLastName()));

        SignUpEntity signUpEntity = modelMapper.map(signUpDAO, SignUpEntity.class);

        SignUpEntity duplicateData = userRepository.findByemail(signUpEntity.getEmail());

        if(duplicateData != null) throw  new RuntimeException(" User Already Exists ");

        SignUpEntity savedData = userRepository.save(signUpEntity);

        SignUpDAO savedDao = modelMapper.map(savedData, SignUpDAO.class);

        SignUpResponse signUpResponse = modelMapper.map(savedDao, SignUpResponse.class);

        return signUpResponse;

    }
}
